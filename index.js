// Number loop start
let inputNumber = prompt("Give me a number:");

console.log("The number you provided is " + inputNumber);

for(i = inputNumber; i>=0; i--){
	if(i <= 50){
		break;
	}

	if(i % 10 === 0){
		console.log("The number is divisible by 10. Skipping the number.");
		continue;
	}

	if(i % 5 === 0){
		console.log(i);
	}	
}
// Number loop end

// String loop Start 
let myString = "supercalifragilisticexpialidocious";
let myConsonant = "";

console.log(myString);

for(x=0;x < myString.length; x++){
	if(	myString[x].toLowerCase() == 'a' || 
	   	myString[x].toLowerCase() == 'e' || 
	   	myString[x].toLowerCase() == 'i' || 
	   	myString[x].toLowerCase() == 'o' || 
	   	myString[x].toLowerCase() == 'u'){
			continue;
	}
	else {
		myConsonant += myString[x];
	}
}

console.log(myConsonant);
// // String loop end
